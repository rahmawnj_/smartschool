<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['user_model']);

        $this->load->helper(['form', 'url']);
        $this->load->library('form_validation');
        $this->load->model(['student_model', 'student_attandance_model', 'teacher_staff_attandance_model', 'user_model', 'class_model', 'teacher_staff_model']);

        if (!$this->session->userdata('status')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible" role="alert">
            <div class="alert-message">
            Login terlebih dahulu!
            </div>
        </div>');
            redirect('auth/login');
        }
    }

    public function index()
    {
        if ($this->session->userdata('role') !== 'admin_absensi' && $this->session->userdata('role') !== 'Viewer' && $this->session->userdata('role') !== 'operator_absensi') {
            show_404();
        }
        $student_attandances = $this->student_attandance_model->groupby_date_student();
        $teacher_staff_attandances = $this->teacher_staff_attandance_model->groupby_date_teacher_staff();

        // Menggabungkan data absensi teacherstaff dan student
        $dates = array_unique(array_merge(array_column($teacher_staff_attandances, 'date'), array_column($student_attandances, 'date')));

        // Mengurutkan tanggal secara ascending
        sort($dates);

        // Inisialisasi data untuk teacherstaff dan student
        $data_teacherstaff = array();
        $data_student = array();

        // Inisialisasi categories
        $categories = array();

        // Mengisi categories dengan tanggal
        foreach ($dates as $date) {
            $categories[] = $date;
        }
        // Mengisi data untuk teacherstaff dan student
        foreach ($dates as $date) {
            // Mengisi data untuk teacherstaff
            $teacherstaff_total = 0;
            foreach ($teacher_staff_attandances as $attandance) {
                if ($attandance['date'] == $date) {
                    $teacherstaff_total = $attandance['total'];
                    break;
                }
            }
            $data_teacherstaff[] = $teacherstaff_total;

            // Mengisi data untuk student
            $student_total = 0;
            foreach ($student_attandances as $attandance) {
                if ($attandance['date'] == $date) {
                    $student_total = $attandance['total'];
                    break;
                }
            }
            $data_student[] = $student_total;
        }

        $date_now = date('Y-m-d');
        $nowTeacherStaff = $this->db->get_where('teacher_staff_attandances', array('date' => $date_now, 'status_hadir !=' => 'Alfa'))->result_array();
        $nowStudent = $this->db->get_where('student_attandances', array('date' => $date_now, 'status_hadir !=' => 'Alfa'))->result_array();
       
        $this->load->view('dashboard/index', [
            'title' => 'Dashboard',
            'nowStudent' => count($nowStudent),
            'nowTeacherStaff' => count($nowTeacherStaff),
            'users' => count($this->user_model->get_users()),
            'classes' => count($this->class_model->get_classes()),
            'teacher_staffs' => count($this->teacher_staff_model->get_teacher_staffs()),
            'students' => count($this->student_model->get_students()),
            'student_data' => json_encode($data_student),
            'teacherstaff_data' => json_encode($data_teacherstaff),
            'categories' => json_encode($categories),
        ]);
    }
}
