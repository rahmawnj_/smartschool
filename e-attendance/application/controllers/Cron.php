<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cron extends CI_Controller 
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['student_attandance_model','Teacher_staff_attandance_model','Teacher_staff_model', 'student_model']);
    }
    
    /**
     * This function is used to update the age of users automatically
     * This function is called by cron job once in a day at midnight 00:00
     */
    public function index()
    {    
        $date_now = date('Y-m-d');
        $day_now = date('l');

        $holiday_student = $this->db->get_where('holidays', ['waktu' => $date_now, 'type' => 'student'])->row_array();;
        if ((!$holiday_student && ($day_now !== 'Saturday')) && (!$holiday_student && ($day_now !== 'Sunday'))) {
            $students = $this->student_model->get_students();
            foreach($students as $student) {
                $attandance = $this->student_attandance_model->get_student_attandace2('date', $date_now, 'student_attandances.id_student', $student['id_student']);
                if (!$attandance) {
                    $this->student_attandance_model->insert([
                        'id_device' => 0,
                        'id_student' => $student['id_student'],
                        'masuk' => 0,
                        'waktu_masuk' => "-",
                        'keluar' => 0,
                        'waktu_keluar' => "-",
                        'status_hadir' => 'Alfa',
                        'ket'  => 'Alfa',
                        'date' => $date_now,
                    ]);
                }
            }
        }

        $holiday_staff_teacher = $this->db->get_where('holidays', ['waktu' => $date_now, 'type' => 'staff_teacher'])->row_array();;
        if ((!$holiday_staff_teacher && ($day_now !== 'Saturday')) && (!$holiday_staff_teacher && ($day_now !== 'Sunday'))) {
           
            $teacher_staffs = $this->Teacher_staff_model->get_teacher_staffs();
            
            foreach($teacher_staffs as $teacher_staff) {
                $attandance = $this->Teacher_staff_attandance_model->get_teacher_staff_attandace2('date', $date_now, 'teacher_staff_attandances.id_teacher_staff', $teacher_staff['id_teacher_staff']);
            
                 if (!$attandance) {
                    $this->Teacher_staff_attandance_model->insert([
                        'id_device' => 0,
                        'id_teacher_staff' => $teacher_staff['id_teacher_staff'],
                        'masuk' => 0,
                        'waktu_masuk' => "-",
                        'keluar' => 0,
                        'waktu_keluar' => "-",
                        'status_hadir' => 'Alfa',
                        'ket'  => 'Alfa',
                        'date' => $date_now,
                    ]);
                }
            }
        }
       
        
    }
}
?>