<!DOCTYPE html>
<html>

<head>
	<title>Log Image</title>
	<link rel="icon" href="<?= base_url('assets/img/logo.png') ?>">
	<link href="<?= base_url('/assets/css/vendor.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('/assets/css/facebook/app.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('/assets/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('/assets/plugins/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('/assets/plugins/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css') ?>" rel="stylesheet" />

	<link href="<?= base_url('assets/plugins/jvectormap-next/jquery-jvectormap.css ') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/plugins/nvd3/build/nv.d3.css ') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/plugins/bootstrap-daterangepicker/daterangepicker.css ') ?>" rel="stylesheet" />
	<style>
		.card {
			flex-direction: row;
			align-items: center;
		}

		#message {
			text-align: center;
			/* rata tengah*/
			line-height: 400px;
			/* sesuaikan dengan tinggi card */
		}

		.card-title {
			font-weight: bold;
		}

		.card img {
			width: 30%;
			border-top-right-radius: 0;
			border-bottom-left-radius: calc(0.25rem - 1px);
		}

		@media only screen and (max-width: 768px) {
			a {
				display: none;
			}

			.card-body {
				padding: 0.5em 1.2em;
			}

			.card-body .card-text {
				margin: 0;
			}

			.card img {
				width: 50%;
			}
		}

		@media only screen and (max-width: 1200px) {
			.card img {
				width: 40%;
			}
		}

		.indicator {
			position: absolute;
			top: 0px;
			right: 0px;
			display: flex;
			align-items: center;
		}

		.status-circle {
			width: 20px;
			height: 20px;
			border-radius: 10px;
			margin-right: 5px;
		}

		.status {
			font-size: 18px;
			font-weight: bold;
		}
	</style>
</head>

<body style="background-color:midnightblue; margin:80px; overflow: hidden;">

	<div class="container">
		<div class="card" style="width: 100%; height: 80vh;">
			<div class="indicator" style="margin: 5px;" class="m-3">
				<div id="status-circle" class="status-circle"></div>
				<span id="status" class="status"></span>
			</div>
			<img id="image" style="display: none; height:100%;" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgKCgkLDRYPDQwMDRsUFRAWIB0iIiAdHx8kKDQsJCYxJx8fLT0tMTU3Ojo6Iys/RD84QzQ5OjcBCgoKDQwNGg8PGjclHyU3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3N//AABEIAJAAsgMBEQACEQEDEQH/xAAcAAEAAgMBAQEAAAAAAAAAAAAABAUBAgYDBwj/xABEEAABAwMBBQUFAwkFCQAAAAABAgMEAAURIQYSMUFRE2FxgZEUFSJioQdCUiMkMoKSorHB0TNVcpOUFhclNFNzw9Lw/8QAGwEBAAIDAQEAAAAAAAAAAAAAAAQFAQIDBgf/xAAzEQEAAgECBAQEBAUFAAAAAAAAAQIDBBEFEiExEzJRoSJBYXEGI5HRQlKBwfAVJDOx8f/aAAwDAQACEQMRAD8A+lVBSSgUCgUCgUCgUCgUCgUCgZwDk4GKCnm7T2qKooS8qU6NC3FSHCO4ngD4mpOHR583krO3si59Zp8H/JeIn3VD22EoqIi21ltPIyHiVDxSkEeiqscfBMs9b2iFXk/EGCPJWZ9v8/RCVtPeSo5egpHINxlaftLNSo4JijzXlDn8Q5Z8uOGo2ivJOUzWvD2ZGK2/0TB/NPs0n8Qaj+SPd6N7UXpCviXAcHQx1A+oXp6GtLcDpPltPs61/EN/4sfvsnR9sXUn86tpx+KM8Fk+SgnHqaiZOC5q+S0T7JmLj2nt54mPdcwNoLXPcDbUkIfVwZeSW1HwChr5E1W5dNmxeeuy0xarDmj8u26zx/i9DXDdI2BWQoFAoFAoFAoFAoFAoHCm24qrzfYtrHZ/20kj4WUHUd6jyFSdNpMmottSOnqi6rV4dLXfJPX0cdcbjNup/PnT2R4R29Gx481efpXo9NwvDh62jef87PLavi+fPvFPhj6d0UlDSMkhKEjkMBNWUzFI3mdohU1ra87VjeZdZs/sU/OZTJvBcjMq1THRo4ofMfu+HHvFUep4la07Yukf9vS6Tg9KVi2frPp6fu6+NszZI6QlFqiKwOLrYcV+0rJqtte1+syuaUrTpWNvt0RbpsdZ5zSg1Fbhv4+ByONzB70jQ+ldMefJinesuWbT4s0bXrEvmLzTjD7rD6d11lwtrxwyOn0Nem0+WM2OLxGzxeqwTgyzjn5Na7o7VaUrSUuIStJ4pUMg+VazET0mN21ZmOsTtLIBSAlL8xIGgCZboA8AFaVEnh+mmfJCbHFNZEbc8vqArxb3pQKBQKBQKBQKBQKAOOtNhz+0d9MImFBwqYQN9w6hlJ69VdBVhoNDbVW3npVXcQ4hTSV9bT8nIR21PrUmO27JdUcrKElZUe816SMmn01eTtDyk4NXrL881mZ9VgmxXnd3vdEzHL4B/Wuc8S08dN3X/SNV3294XexGzy5VwcnXFhaG4bm4004MZdHFRHy8B3+FQNfrIy7Vx9vmteGcOnBvfL5n0Ycc91Vi5ZoB7qD5JPiv3faq5NWxkvLVJOTwSgYAyTyq602opptNHN369HndZo8mr1c8nSI2iZXsT7PlqbzMueHPwss6DzOtR78UzT2jZKpwXTxG15mUS6bDXCI0p2A+mclOpbUncWfDkTXXFxS2/wCZHRxz8Epy74pnf6uS9tjJ+FchCFDQpWMKSehB51aRqcUxvzKOdJqInbll9TFeGfQygUCgUCgUCgUCgY3tKCn2qui4FuDcVR9vlK7KKlIyrf0OccwOY8q66fF4lus7Q458vh16RvKHsvskiS65/tEV+1pPaLiE43s/fJ+9r00HCrW+rtyRjxfDWPdWY+H0m85s3xWn2fQosZiI2luKy2ygDG62kAVD+srDbZ7DjQVWzWDY4bh1U8jtVnqpRyT60FZt9f5uzVpi3OIwHmETWkzdM9nHOd5XrujzoLZi+Wp+CJrNyiqjKTvB3tU4x1oIcvaGK9Zm5lofblmWrsYfZnIcWdPQak9woPSxW+Bs7Eag+0NCS8StxbiwFvuE5UceNJtM92IiI7LmjIeVBAcslqdcU47bY63FkqUotA7xPE06tNoVYqCmFAoFAoFAoFAoGM6Zx30EeVMRFUgvNuhpQwXQjeQjuVjUeOMAccVmIYZ2ZtjU+YraaRh4uo3LdzDUfX4h8zn6R6ApHI1Kx15Y2cbzvK8uMD2vccaWWZTOrLw+71BHMHmK3altne07zL6Oxls6OtE+ik9UnrQTs9KClS45ZXHGnm3F25aytp1tBV2JOpQoDUDPAjlxxjUKPaD7QrJBLkNtBuSiMOJax2evIqOhPcBXO2WsLDR8K1Wrjmxx09Z6ONt3+7qdLzJsDsV5RyhgOEsuK5DdyEgnvAHfSuStm+q4VqtPXmtG9fWO37/2dzEhMwLj7XJZZaRbIanER2RhuMFaBKe/AVk444rdWPnG0cSDe3tnb1Gua5W0FynNrSy28FBlvOd3d+6EgeetGX3VAUEgKOSBgnrWWGaAN3Gi9PGs7DnBUBJKBQKBQKBQKBQM41OPOgjCI7enZEFDjjMFrLUp5vRbhIyWkHGgwfiUNRvYBCs47Y8fzlztZ6u7FWhIS5bY6IkhAwF430q46rB1JySd4EL+apDk2gz5doecjX8KbhpAUzOU4VtdClSzqnkQV9SN5VBbXGH7UG5EZwNS2tWXgMg/KeqTQb22cJYcbdbLMpk4eZP3D1HUHiDQRdrZrtu2auMuPkPNsK7MjiknQHyznyrFp2h0xU8TJWnrMQ/PIGAUjvPfUHvG76fSlccclOkR/Z3W0dt2Sa2NYkWyS2u4breCl7Li1HG9vIzpzPAYwK63isUiY7vPaTNr8mttj1Eflzvv06bfLZ1exSzebOW7iSTNt7bZPNSUKWgnPPimu9Z5oiXlNVjjFnvjjtEyxsV9mlo2TnKnMvPzJWNxtx/dHZp7gBx7/wCFbuO7uNP5ZNGGri0NtlxxQS2kEqWTgJA4knpRiez5i79qrynVqhW2M9FKiWXVykoK0fdJSdQSMaVJrosloiYtCFfiOGlprMT0+jrRVQtygUCgUCgUCgUGjqlNtLWnd3kpKhvcNOtA2edukKyQw7aUv7yO0UYslKlKUs7ylEObmMkk6E1Njsjz3WfvqOhREpmZGUnVRejL3E/rgFP1rLCRFuUGcnMSZHfHA9m4FUEP3OqGS5ZHxD5qjFO9HV+p9096SO8GgsmmwVB11toSN3dUpGpA6ZwCRmg0uMNm4wJEKSkqZfbU2sDjgjGnfQ326w+AX3Z64WWfIiPtreajqCfaWkEoORkZP3VYwSnjrnXQ1EnFaJe70HG9Pnptmty2+vb9SwbO3S/SENQIqw2o/FIWghtscznn4ClcczLbWca02Ck+Fbmt8oj932tux+77bBYtZSl+CjdaUvg4PvJV0CvocHlUvbbo8Je1r2m9p6ylxLxFcWGJCjElc48j4VZ+U8FDvGaNUmXOiQ2+1lSmWUDmtYFBVqbevzie1bcYtSFBZQ4kpXMI1AKTqlvgcHBVwICRhQWCrdBWoqXCjqUTkksgknxxWeb6yx/nZUCoCUUCgUCgUCgUCgwpIWkoVwUN0+dBZbOOF2xQFK/T7BKVdygMEeRBqbHZHnussddTyrLCNLt8KbgzIcZ8jh2rQWR5mg8GrNFYdS4wX2yDncRIXueG6TgeQoLDjrwoId2mGDCU62jtXlEIZazjtHDokeHU8gCaBaoIt8VLRWXHVEredI1ccJypR8T6AAUEygE454oPCSiLIaLUoMuNni26EqHoaCLFg2eE52kSNAjr4bzTaEHHiMUEwSGOb7Wf+4KDPtDH/Xa/bFBQCoKSUCgUCgUCgUCgUGuykpbcy4290JSn2lx2Ng8UkgrHiFHPgsVLpPwuF+7pc1u1KBQDgDJ4UFSgG4XxTitWIA3UdFPKHxHyT8P6yqC1SMCgzQQ56ILwQ3OWga5SFOlGfqM0GiLRbCkEQo6weBKd7+NBn3Ra/wC7of8AkJ/pQZ9z2z+7of8Ap0f0oHui2f3fC/06f6UFSKgpJQKBQKBQKBQKAOOlB4QIglS7jHS52UlLrUth3m2VI3B4gltWRzBqThnerjfuvLXOMtC232wzMYO6+znODyUDzSeIP8wRXVonZoFBGuMsQYL8jd3lNp+FP4lcAPMkCg1tUT2KC0yo7zmN5xZ4qWdVH1JoJdAJxQQ3FwZUpUV4MuPoGdxxOuOozy8KDyNmhpUTHDsZZ4lhwpJoMezXNj/l5zT4/DJaxp4poMe8JUc/nltdSnhvx1BwDvxof40D35bhoXl/5Dn/AK0FeKgpJQKBQKBQKBQKBQeBeRCvEGStQSh4mI5k4/TIKP3wEjvcNdcM/JpeOi7uMEySiRHc7Ca1kNPY5c0qHNJ6eYwcVJcSDcUvOezSWxGmIHxMk53h+JB+8PqNM4oJ9BWXL85uMKEdUpUZDg5YTokHzP0oLMDFAoFBFmQI81AQ+3qDlLiThSD1BGooIZfm2wYmBcqKDpIbTlxA+dI4jvHpzoLJh9p9lD7LiHG3BlK0KBBHiKDdWMa+OtAwev0NBzoqCklAoFAoFAoFAoPGW6402kR0pVIdWG2Uq4b55nuAyfKtqxvOzEztG6anZm3qhOsyUKekOtlK5rhBdBI/SSfu4OoxgA8Kl1rEOEzMpdkmOTIZRLCBOjrLEtCRp2gA1A5BQKVj5VDOtZYSJkFic12UlGQk7yFJJCkHqlQ1B7waCD/xW35yg3ON1BSiQnxGiV/unxNB42e4xJ17nFLpS+lCG0sPILTgSBkncUAcZJ1xigvc0CgHSgUGFZPCgqZcH2Iuz7c83EXqt1Dp3Y7nUr/CfmHnvYxQQom0qrsoRbRFBl7m+pcleGUpzjfSoavJ4Ebmh0BUkmgme6Lir4l7SXFKjqQ3HjhIPcC0TjxJPeaCIKgpJQKBQKBQKBQKyI0xaWHIUpZ/Jx5SXFnoCFIJ8t/J7hW2OdrNbdnV5BOP/sVLcFRc2lwJSbwwlSglsNzWkDJcaByFDGpUgknA4gqGCcYC2acQ82h1tYWhSQUqScgjrQbEUEadAh3BoNTojMhAOQl5sKAPUZ4fSghCylnSBcrjFTzQXg+k+ToUQO5JFBlLF9a0E22voHAKirbPmQsj6UDtL8k/DAtZ7/bnB/4j/Gg2C76vRUa2NfMJDjn03E/xoNTCvL2RJvDbSTw9iiBCh5uKWPpQZbsEAOoektqmvoO8l2YsvFCuqQrRJ/wgUEm422PcW0JlAlbSt9l5B3XGl40UlQ4HiD1BIOQSKCsMbahJ3W7haVoGiVvQ3N9Q6q3XAM9cADoBQeQqCklAoFAoFAoFAoNVpS4hSHEhaFApUlQyFA6EVkRtntpYrVwescx87zKtyNIcOjugJQT+NPDvx4gTq47+FGTboh2zY/FnFE/F3ddnkONYb7qYg2JxS0ZNrWSpSAM+yqJ1I+Q/Tw4BcIUlaUrSoEKGQQcgigzQKBQKBQM64oNC80k4LiAe9QoNwQRkajqKBQc4KgpJQKBQKBQKBQKDylPpixXpKxlLLanCOoAzTbfoPmQBWg9uQsrJUvI/SJOSfU5r3OHDFMUY47Q+eajUTkzWy79Zlc23aa821AaZlh9lOgbkjfwO5XH1zUPLwvHad6TyrHDxrLSNrxzeyfI26vL7RbQ3DYyNVAFf0OK5V4R163d78d3j4adfug7MbVTbJMciyVuS4C/ygQr+0aydd3HLPL0rjm4d8X5TtpuLxNYnN9t30+2XOFc4wfgvpdb54Oqe4jkarJiYmYldVtFo5onol7wrDZmgUHhNlMxGC6+ohIIAAGSo8gBzNBBTEmXD457q47J4RmVYOPnUOfcPWg9k2W2JTgwWF9607x9Tk0HmqzNNELtzrkN3luKKkeaCcY8MUEf2y/o+D3Mw5u6b6JQSFd4B4CgjioKSUCgUCgUCgUCgiXdhUm1TWG9VuMLSkdSUnFbVna0SxMbxL5yhQcQlY4ECveUtFqxMPm96TS01n5M1s0KDG6krDhH5QDGe6sTHXdvzzy8qMt2fbJvvKzvuNOgflAg6LHUjgap+J6G+SfGxd/nHq9V+HeK6bH/tdbHwT2n+Wf2ddYvtURuIRe4pJOEh6KM7x/wcc9wqhjL3i3TZ7LV8Evip42K8Wo7+Je7bKxuS0IcVwQ7ltR8lYramSmTyTv8AZSbTCeeGtbsKphIn3l+QvVqEeyaTy7QgFSvQgDxNBbUCgHh/Cg10+X0NBzwqCklAoFAoFAoFAoA69KDj9odn3o7q5ltbLrTnxOxkaqQo8SkcweOPTpV1w7ifhR4eXt6qHinCfHnxcPf5x6uebWhwqCFAlJwtPNJ6EcQfGvR0yUvXmrO7yuTFkx2mt42ltW7mxjTI1HWm40acS85uMbz6xxSwguEeQ4edR8uqw44+K0JWDR6jLPwUmVps/a/Z5j10kRnoklDim21ORw82MYzvBJJSoHIzppXz/j+tpnzzjxRHL0+kzP19XuOF4s+n00Yslp9du8R9nVokvSkHfiRLggDVUV8En9VXD9qvNeHWk/DM0n6/usPl1hDuO43AkCEm5WqVuktrbaWQFcshG8CKnabW6ml43yRav3/djlpPWYWP2WXVy5bOky1qVLDpW7vnKiFYKVHx/lXr7Y706X7oeTLp8uS06fy77OxzWrVnNBq6tDaStxQShIJUVHAA6k0nox3cg79otoQ4tLce5OoCiEuNs5SsdRrwNSY0maY35ZQ7cQ09ZmJunCqhbFBmgxigUCgUCgUCgzpnx499BCn2m3XBQVMhsOrAwFqbG8PA8RXSmS9PLMw0vipfzRv90E7J2UqyY7/gJboHpvV3jW6iP45/VHnQaXv4cfo9W9m7K3g+7IzhHAvJ7U+qs1yvqM1/NaZ/rLrTT4aeSkR9lm2htpAQ22hCRwCRgeg0Fcd53deynlR5D7F1tsV9UWS4rtmHASMpUd7iNQM7yTjUVSa+tcOqrlvG9f2da9a8rndmrBPduKZEyOwqElx1D5ecDpUpJUj8mMZB3h+lpp1zptr9XgnFMY94tO0xt07xv1/8YpWd1ztVFbhWV9xh+albqktJAlulI3lYPwlWNAa58GidXr8eO8RMb79o36R9nHX5Jw6a+SJ7Q5y2z5VolpmW9YS4kbpQdULR+Ejp38RX1LVaWuesRPeOzwui12TTWmY6xPd28H7QICkAXCDMjOY1KEdsg+BTrjxSKpLcP1FJ2iN3o8fFtJk68+33Jn2hW9CSIUGbIVyKkhpPmVHP7prNeH6i07TXb7mTiukp/Fv9nI3vaC53z4JriWopORFZHwd28TqvzwOeNKs9Pw2mKYtfrPspdXxfJmia4+lfdXh1QGAE48/61Zf1U/T0f//Z" class="card-img-top" alt="...">
			<div class="card-body">
				<h1 id="message" class="text-center">Welcome</h1>

				<h1 id="res-title" class="card-text"></h1>
				<h2 id="res-name" class="card-title"></h2>
				<h2 id="res-type" class="card-text"></h2>
				<!-- <p id="res-address" class="card-text"></p> -->
				<!-- <p id="res-berlaku" class="card-text"></p>
				<p id="res-jatuhtempo" class="card-text"></p> -->
			</div>
		</div>
	</div>

	<!-- <script src="<?= base_url('assets/landingpage/bootstrap-5.3.0-alpha1-dist/js/bootstrap.bundle.min.js') ?>" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script> -->
	<script>
		let lastImageUrl = '';
		let intervalId;
		let timeoutId;
		let isImageShown = false;
		let time = 0;

		document.addEventListener("DOMContentLoaded", function() {
			intervalId = setInterval(fetchData, 500);
		});

		function fetchData() {
			var now = new Date();

			fetch('<?= base_url('api/last_log/' . $id) ?>')
				.then(response => response.json())
				.then(data => {
					var compareDate = new Date(data.device.time);
					// if (nowTime - data.logs.time_heartbeat > 30) {
					// 	document.getElementById("status").innerHTML = "Offline";
					// 	document.getElementById("status-circle").style.backgroundColor = 'red';
					// } else {
					// 	document.getElementById("status").innerHTML = "Online";
					// 	document.getElementById("status-circle").style.backgroundColor = 'green';
					// }
					if (now.getTime() - compareDate.getTime() < 20000) {
						console.log('kurang dr 20')

						if (data.data) {
							// Simpan URL gambar yang didapat dari API
							lastImageUrl = $this - > db - > get_where('settings', ['name' => 'url_storage_fe']) - > row_array()['value'].
							'/assets/img/uploads/' + data.data.foto;
							// Tampilkan gambar yang didapat dari API
							document.getElementById("message").innerHTML = '';
							document.getElementById("image").src = $this - > db - > get_where('settings', ['name' => 'url_storage_fe']) - > row_array()['value'].
							'/assets/img/uploads/' + data.data.foto;
							document.getElementById("image").style.display = 'block';
							document.getElementById("image").style.height = '50%';
							document.getElementById("image").style.width = '25%';
							document.getElementById("image").style.marginLeft = '10%';
							document.getElementById("image").style.marginBottom = '5%';
							isImageShown = true;
							// Tampilkan response text
							// if (data.logs.operator === 'VerifyPush') {
							// 	let checkExpire = '';

							// 	if (new Date() >= new Date(data.data_user.jatuhtempo)) {
							// 		checkExpire = 'Status Member : Kadaluarsa';
							// 	} else {
							// 		if (data.data_user.keterangan.includes("Personal Trainer")) {
							// 			checkExpire = 'Status Member : kadaluarsa';
							// 		} else {
							// 			checkExpire = 'Status Member : Aktif';
							// 		}
							// 	}

							document.getElementById("res-title").innerHTML = '<?= $this->db->get_where('settings', array('name' => 'school_name'))->row_array()['value'] ?>';
							document.getElementById("res-title").style.marginBottom = '10px';
							document.getElementById("res-title").style.textAlign = 'center';
							document.getElementById("res-name").innerHTML = 'Nama : ' + data.data.nama;
							document.getElementById("res-name").style.textAlign = 'center';
							var type = null
							if (data.type == 'student') {
								type = 'Kelas : ' + data.data.kelas

							} else {
								type = 'Jabatan : ' + data.data.jabatan

							}

							document.getElementById("res-type").innerHTML = type
							document.getElementById("res-type").style.textAlign = 'center';

							// 	// document.getElementById("res-address").innerHTML ='Alamat : ' + data.data_user.alamat;
							// 	// document.getElementById("res-jatuhtempo").innerHTML = data.data_user.jatuhtempo;
							// 	// document.getElementById("res-berlaku").innerHTML = 'MASA AKTIF';
							// } else {
							// 	document.getElementById("res-title").innerHTML = 'TFITNESS';
							// 	document.getElementById("res-name").innerHTML = 'TIDAK TERDAFTAR';
							// 	// document.getElementById("res-berlaku").innerHTML = '';
							// 	// document.getElementById("res-jatuhtempo").innerHTML = '';
							// 	// document.getElementById("res-address").innerHTML = '';
							// 	document.getElementById("res-type").innerHTML = '';
							// 	document.getElementById("res-type").style.textAlign = 'center';
							// 	document.getElementById("res-name").style.textAlign = 'center';
							// 	document.getElementById("res-title").style.textAlign = 'center';

							// }
							// Reset timer untuk menampilkan gambar terakhir
							clearTimeout(timeoutId);
							timeoutId = setTimeout(hideImage, 2000);
							time = 0;
						}
						// document.getElementById("image").src = data.device.foto;
					}
				});
		}

		function hideImage() {
			document.getElementById("image").style.display = 'none';
			isImageShown = false;
			document.getElementById("message").innerHTML = 'Welcome';
			document.getElementById("res-type").innerHTML = '';
			document.getElementById("res-name").innerHTML = '';
			document.getElementById("res-berlaku").innerHTML = '';
			document.getElementById("res-jatuhtempo").innerHTML = '';
			document.getElementById("res-title").innerHTML = '';

		}
	</script>


</body>

</html>