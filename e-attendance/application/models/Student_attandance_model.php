<?php
class Student_attandance_model extends CI_Model
{
    public function get_student_attandances()
    {
        $this->db->where(['students.deleted' => 0, 'id_student_attandance !=' => 0]);        
        $this->db->join('students', 'students.id_student = student_attandances.id_student');
        $this->db->join('classes', 'classes.id_class = students.id_class');
        $this->db->select('student_attandances.*, students.nama as student_nama,  kelas as student_kelas');
        return $this->db->get('student_attandances')->result_array();
    }
    public function get_student_attandace($date, $id_student)
    {
        // $this->db->join('students', 'students.id_student = students.id_student');
        return $this->db->get_where('student_attandances', array('student_attandances.date' => $date, 'id_student' => $id_student))->row_array();
    }
    public function get_student_attandace2($field, $id_student_attandance, $field2, $id_attandance2)
    {
        // $this->db->join('students', 'students.id_student = student_attandances.id_student');
        return $this->db->get_where('student_attandances', array($field => $id_student_attandance, $field2 => $id_attandance2))->row_array();
    }

    public function insert(...$data)
    {
        $this->db->insert('student_attandances', $data[0]);
    }

    public function update($id_student_attandance, ...$data)
    {
        $this->db->update('student_attandances', $data[0], ['id_student_attandance' => $id_student_attandance]);
    }

    public function delete($id_student_attandance)
    {
        // $this->db->where('id_student_attandance', $id_student_attandance);
        // $this->db->delete('student_attandances');

        $this->db->update('student_attandances', ['deleted' => 1], ['id_student_attandance' => $id_student_attandance]);

    }

    public function groupby_date_student()
    {
        return $this->db->query('SELECT date, COUNT(*) AS total
        FROM student_attandances
        WHERE status_hadir != "Alfa"
        GROUP BY date;
        ')->result_array();
    }
}