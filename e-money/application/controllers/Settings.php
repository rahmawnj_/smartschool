<?php
defined('BASEPATH') or exit('No direct script access allowed');

class settings extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library(["form_validation", 'session']);
        $this->load->model(['setting_model']);
        $this->load->helper(['form', 'url']);
        if (!$this->session->userdata('status')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible" role="alert">
            <div class="alert-message">
            Login terlebih dahulu!
            </div>
        </div>');
            redirect('auth/login');
        }
    }

    public function index()
    {
        if ($this->session->userdata('role') !== 'administrator') {
            show_404();
        }
        $data = [
            'title' => 'Setting',
            'settings_secret_key' => $this->setting_model->get_setting('name', 'secret_key')[0]
        ];

        $this->load->view('user/settings/index', $data);
    }


    public function update()
    {
        if ($this->session->userdata('role') !== 'administrator') {
            show_404();
        }
        $this->form_validation->set_rules('secret_key', 'Secret Key', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->index();
        } else {
            $this->setting_model->update($this->input->post('id_setting'), [
                'value' => $this->input->post('secret_key'),
            ]);
            $this->session->set_flashdata('success', 'Setting Berhasil Diperbarui!');
            redirect('settings');
        }
    }
}
