<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Data extends CI_Controller
{
    public $data = null;
    public function __construct()
    {
        parent::__construct();
        $this->load->library("form_validation");
        $this->load->model(['student_model', 'class_model', 'product_model', 'cart_model']);
        $this->load->helper(['form', 'url']);
        $this->data = &get_instance();
    }

    public function get_siswa($id_siswa)
    {
        echo json_encode($this->student_model->get_student('id_student', $id_siswa));
    }

    public function search_rfid()
    {
        $data = $this->student_model->get_student('rfid', $this->input->post('rfid'));
        if (count($data) !== 0) {
            echo json_encode($data);
        } else {
            echo json_encode(['error' => 'siswa tidak ditemukan']);
        }
    }
    public function search_nama()
    {
        // $data = $this->student_model->get_student('nama', $this->input->post('nama'));
        $this->db->where('nis', $this->input->post('nama_nis'));
        $this->db->or_where('nama', $this->input->post('nama_nis'));

        $data = $this->db->get('students')->result_array();
        if (count($data) !== 0) {
            echo json_encode($data);
        } else {
            echo json_encode(['warning' => 'tidak ditemukan']);
        }
    }

    public function stock($id_product)
    {
        echo json_encode($this->product_model->get_product('id_product', $id_product)[0]['stok']);
    }

    public function search_rfid_nis()
    {
        $this->db->where('nis', $this->input->post('rfid_nis'));
        $this->db->or_where('rfid', $this->input->post('rfid_nis'));
        $data = $this->db->get('students')->result_array();
        if (count($data) !== 0) {
            echo json_encode($data);
        } else {
            echo json_encode(['error' => 'siswa tidak ditemukan']);
        }
    }
}
