<?php $this->load->view('layout/head') ?>

<div class="wrapper">
    <?php $this->load->view('layout/sidebar') ?>
    <div class="main">
        <?php $this->load->view('layout/header') ?>

        <main class="content">
        <div class="container-fluid p-0">
            <div class="row removable">
                <div class="col-lg-12">
                    <div class="card flex-fill">
                        <div class="card-header d-flex justify-content-between">
                            <h5 class="card-title mb-0"><?= $title ?></h5>
                            <a href="<?= base_url('dashboard/classes/create') ?>" class="btn btn-primary float-right fas fa-plus"></a>
                        </div>

                        <div class="card-body">
                            <div class="table-responsive">

                                <div class="flash-data-success" data-flashdatasuccess="<?= $this->session->flashdata('success') ?>"></div>
                                <div class="flash-data-error" data-flashdataerror="<?= $this->session->flashdata('error') ?>"></div>
                                <table id="table" class="table table-hover my-0 " style="width: 100% ;" >
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Kelas</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 0;
                                        foreach ($classes as $class) : ?>
                                            <tr>
                                                <td><?= ++$no; ?></td>
                                                <td><?= $class['kelas'] ?></td>
                                                <td>
                                                    <a class="fas fa-location-arrow btn btn-sm bg-primary text-white" href="<?= base_url('dashboard/classes/students/' . $class['id_class']) ?>"></a>
                                                    <a class="fa fa-edit btn btn-sm bg-warning text-white" href="<?= base_url('dashboard/classes/edit/' . $class['id_class']) ?>"></a>
                                                   
                                                    <a id="delete-button" class="fas fa-trash btn btn-sm bg-danger text-white" href="<?= base_url('classes/delete/' . $class['id_class']) ?>"></a>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </main>

        <?php $this->load->view('layout/footer') ?>
    </div>
</div>

<?php $this->load->view('layout/foot') ?>

