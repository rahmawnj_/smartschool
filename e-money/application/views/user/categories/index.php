

<?php $this->load->view('layout/head') ?>

<div class="wrapper">
    <?php $this->load->view('layout/sidebar') ?>
    <div class="main">
        <?php $this->load->view('layout/header') ?>

        <main class="content">
            <div class="container-fluid p-0">
                <div class="row removable">
                    <div class="col-lg-12">
                        <div class="card flex-fill">
                            <div class="card-header d-flex justify-content-between">
                                <h5 class="card-title mb-0"><?= $title ?></h5>
                                <a href="<?= base_url('dashboard/categories/create') ?>" class="btn btn-primary float-right fas fa-plus"></a>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <div class="flash-data-success" data-flashdatasuccess="<?= $this->session->flashdata('success') ?>"></div>
                                    <div class="flash-data-error" data-flashdataerror="<?= $this->session->flashdata('error') ?>"></div>


                                    <table id="table" class="table table-hover my-0 ">

                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Kategori</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $no = 0;
                                            foreach ($categories as $category) : ?>
                                                <tr>
                                                    <td><?= ++$no; ?></td>
                                                    <td><?= $category['kategori'] ?></td>
                                                    <td>
                                                        <!-- <a class="fas btn-sm fa-location-arrow btn bg-primary text-white" href="<?= base_url('dashboard/categories/products/' . $category['id_category']) ?>"></a> -->
                                                        <a class="fa fa-edit btn btn-sm bg-warning text-white" href="<?= base_url('dashboard/categories/edit/' . $category['id_category']) ?>"></a>
                                                        <!-- <a class="fas fa-eye btn-sm btn bg-info text-white" href="<?= base_url('dashboard/categories/' . $category['id_category']) ?>"></a> -->
                                                        <a id="delete-button" class="fas fa-trash btn-sm btn bg-danger text-white" href="<?= base_url('categories/delete/' . $category['id_category']) ?>"></a>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </main>

        <?php $this->load->view('layout/footer') ?>
    </div>
</div>

<?php $this->load->view('layout/foot') ?>