<?php
defined('BASEPATH') or exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/userguide3/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['default_controller'] = 'auth';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['dashboard/users'] = 'users';
$route['dashboard/users/create'] = 'users/create';
$route['dashboard/users/(:any)'] = 'users/view/$1';
$route['dashboard/users/edit/(:any)'] = 'users/edit/$1';

$route['dashboard/students'] = 'students';
$route['dashboard/students/create'] = 'students/create';
$route['dashboard/students/(:any)'] = 'students/view/$1';
$route['dashboard/students/edit/(:any)'] = 'students/edit/$1';

$route['dashboard/teacher_staffs'] = 'teacher_staffs';
$route['dashboard/teacher_staffs/create'] = 'teacher_staffs/create';
$route['dashboard/teacher_staffs/(:any)'] = 'teacher_staffs/view/$1';
$route['dashboard/teacher_staffs/edit/(:any)'] = 'teacher_staffs/edit/$1';

$route['dashboard/classes'] = 'classes';
$route['dashboard/classes/create'] = 'classes/create';
$route['dashboard/classes/(:any)'] = 'classes/view/$1';
$route['dashboard/classes/students/(:any)'] = 'classes/students/$1';
$route['dashboard/classes/edit/(:any)'] = 'classes/edit/$1';

$route['dashboard/merchants'] = 'merchants';
$route['dashboard/merchants/create'] = 'merchants/create';
$route['dashboard/merchants/(:any)'] = 'merchants/view/$1';
$route['dashboard/merchants/products/(:any)'] = 'merchants/products/$1';
$route['dashboard/merchants/edit/(:any)'] = 'merchants/edit/$1';

$route['dashboard/categories'] = 'categories';
$route['dashboard/categories/create'] = 'categories/create';
$route['dashboard/categories/(:any)'] = 'categories/view/$1';
$route['dashboard/categories/products/(:any)'] = 'categories/products/$1';
$route['dashboard/categories/edit/(:any)'] = 'categories/edit/$1';

$route['dashboard/roles'] = 'roles';
$route['dashboard/top_ups'] = 'top_ups';
$route['dashboard/top_ups/create'] = 'top_ups/create';
$route['dashboard/top_ups/(:any)'] = 'top_ups/view/$1';
$route['dashboard/top_ups/change_pin/(:any)'] = 'top_ups/change_pin/$1';
$route['dashboard/top_ups/students'] = 'top_ups/students';
$route['dashboard/check_balance'] = 'dashboard/check_balance';

$route['dashboard/devices'] = 'devices';
$route['dashboard/devices/create'] = 'devices/create';
$route['dashboard/devices/(:any)'] = 'devices/view/$1';
$route['dashboard/devices/products/(:any)'] = 'devices/products/$1';
$route['dashboard/devices/edit/(:any)'] = 'devices/edit/$1';

$route['dashboard/student_parking_transactions'] = 'student_parking_transactions/index';
$route['dashboard/student_parking_transactions/view/(:any)'] = 'student_parking_transactions/view/$1';

$route['dashboard/teacher_staff_parking_transactions'] = 'teacher_staff_parking_transactions/index';
$route['dashboard/teacher_staff_parking_transactions/view/(:any)'] = 'teacher_staff_parking_transactions/view/$1';

$route['dashboard/products'] = 'products/index';
$route['dashboard/products/create'] = 'products/create';
$route['dashboard/products/(:any)'] = 'products/view/$1';
$route['dashboard/products/edit/(:any)'] = 'products/edit/$1';

$route['dashboard/order_transactions'] = 'order_transactions/index';
$route['dashboard/order_transactions/view/(:any)'] = 'order_transactions/view/$1';

// merchants
$route['merchant/dashboard'] = 'dashboard/merchant_dashboard';
$route['merchant/dashboard'] = 'dashboard/merchant_dashboard';
$route['merchant/dashboard'] = 'dashboard/merchant_dashboard';
$route['merchant/dashboard'] = 'dashboard/merchant_dashboard';
$route['merchant/check_balance'] = 'dashboard/merchant_check_balance';

$route['merchant/catalogs'] = 'products/catalogs';
$route['merchant/order_transactions/report'] = 'order_transactions/report';
$route['merchant/order_transactions/merchant_view/(:any)'] = 'order_transactions/merchant_view/$1';

$route['merchant/order_transactions'] = 'order_transactions/merchant_index';
$route['merchant/order_transactions/view/(:any)'] = 'order_transactions/merchant_view/$1';

$route['merchant/products'] = 'products/merchant_index';
$route['merchant/products/create'] = 'products/merchant_create';
$route['merchant/products/(:any)'] = 'products/merchant_view/$1';
$route['merchant/products/edit/(:any)'] = 'products/merchant_edit/$1';

$route['merchant/carts/edit'] = 'carts/edit';
$route['merchant/carts/view'] = 'carts/view';

$route['merchant/categories'] = 'categories/categories';
$route['merchant/categories/products/(:any)'] = 'categories/merchant_product/$1';
