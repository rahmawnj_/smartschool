<?php
class Purchased_item_model extends CI_Model
{
    public function get_items()
    {
        $this->db->join('order_transactions', 'order_transactions.id_order_transaction = purchased_items.id_order_transaction');
        $this->db->join('products', 'products.id_product = purchased_items.id_product');

        return $this->db->get('purchased_items')->result_array();
    }

    public function get_item($field, $id_order_transaction = null)
    {
        $this->db->join('order_transactions', 'order_transactions.id_order_transaction = purchased_items.id_order_transaction');
        $this->db->join('products', 'products.id_product = purchased_items.id_product');
        return $this->db->get_where('purchased_items', array($field => $id_order_transaction))->result_array();
    }

    public function insert(...$data)
    {
        $this->db->insert('purchased_items', $data[0]);
    }

}
