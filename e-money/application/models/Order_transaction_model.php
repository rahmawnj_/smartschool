<?php
class Order_transaction_model extends CI_Model
{
    public function get_order_transactions()
    {
        $this->db->select('order_transactions.*, merchants.nama as merchant_nama');
        $this->db->join('merchants', 'merchants.id_merchant = order_transactions.id_merchant');
        return $this->db->get('order_transactions')->result_array();
    }

    public function get_order_transaction($field, $where)
    {
        $this->db->select('order_transactions.*, merchants.nama as merchant_nama');
        $this->db->join('merchants', 'merchants.id_merchant = order_transactions.id_merchant');
        return $this->db->get_where('order_transactions', [$field => $where])->result_array();
    }

    public function get_otr($id_merchant)
    {
        $this->db->order_by('created_at', 'asc');
        return $this->db->get_where('order_transactions', ['order_transactions.id_merchant' => $id_merchant])->result_array();
    }

    public function get_by_date($startDate = null, $endDate = null)
    {
        if ($startDate && $endDate) {
            $this->db->where('order_transactions.created_at >=', date('Y-m-d', strtotime($startDate)));
            $this->db->where('order_transactions.created_at <=', date('Y-m-d', strtotime($endDate)));
            $this->db->select('order_transactions.*, merchants.nama as merchant_nama');
            $this->db->join('merchants', 'merchants.id_merchant = order_transactions.id_merchant');

            return $this->db->get('order_transactions')->result_array();
        } else {

            $this->db->select('order_transactions.*, merchants.nama as merchant_nama');
            $this->db->join('merchants', 'merchants.id_merchant = order_transactions.id_merchant');
            return $this->db->get('order_transactions')->result_array();
        }
    }

    public function insert(...$data)
    {
        $this->db->insert('order_transactions', $data[0]);
    }

    public function get_last_order_transaction($id_merchant)
    {
        $this->db->order_by('created_at', 'desc');
        return $this->db->get_where('order_transactions', ['id_merchant' => $id_merchant])->result_array();
    }
}
