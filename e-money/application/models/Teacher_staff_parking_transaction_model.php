<?php
class Teacher_staff_parking_transaction_model extends CI_Model
{
    public function get_teacher_staff_parking_transactions()
    {
        $this->db->select('teacher_staff_parking_transactions.*, teacher_staffs.nama as teacher_staff_nama, teacher_staffs.foto as teacher_staff_foto, teacher_staffs.rfid as teacher_staff_foto, devices.device as device_nama, devices.harga as device_harga ');
        $this->db->join('devices', 'devices.id_device = teacher_staff_parking_transactions.id_device');
        $this->db->join('teacher_staffs', 'teacher_staffs.id_teacher_staff = teacher_staff_parking_transactions.id_teacher_staff');

        return $this->db->get('teacher_staff_parking_transactions')->result_array();
    }

    public function get_teacher_staff_parking_transaction($field, $where)
    {
        $this->db->select('teacher_staff_parking_transactions.*, teacher_staffs.nama as teacher_staff_nama, teacher_staffs.foto as teacher_staff_foto, teacher_staffs.rfid as teacher_staff_foto, devices.*');
        $this->db->join('devices', 'devices.id_device = teacher_staff_parking_transactions.id_device');
        $this->db->join('teacher_staffs', 'teacher_staffs.id_teacher_staff = teacher_staff_parking_transactions.id_teacher_staff');
        return $this->db->get_where('teacher_staff_parking_transactions', [$field => $where])->result_array();
    }

    public function get_ptr($id_device)
    {
        $this->db->order_by('created_at', 'asc');
        return $this->db->get_where('teacher_staff_parking_transactions', ['teacher_staff_parking_transactions.id_device' => $id_device])->result_array();
    }

    public function get_by_date($startDate = null, $endDate = null)
    {
        if ($startDate && $endDate) {
            
            $this->db->where('teacher_staff_parking_transactions.created_at >=', date('Y-m-d', strtotime($startDate)));
            $this->db->where('teacher_staff_parking_transactions.created_at <=', date('Y-m-d', strtotime($endDate)));
            $this->db->select('teacher_staff_parking_transactions.*, devices.device as device, students.nama as student_nama, students.nis as student_nis');
            $this->db->join('devices', 'devices.id_device = teacher_staff_parking_transactions.id_device');
            $this->db->join('students', 'students.id_teacher_staff = teacher_staff_parking_transactions.id_teacher_staff');
            return $this->db->get('teacher_staff_parking_transactions')->result_array();
        } else {
            $this->db->select('teacher_staff_parking_transactions.*, devices.device');
            $this->db->from('teacher_staff_parking_transactions');
            $this->db->join('devices', 'devices.id_device = devices.id_device');
            return $this->db->get()->result_array();
        }
    }

    public function insert(...$data)
    {
        $this->db->insert('teacher_staff_parking_transactions', $data[0]);
    }

    public function get_last_teacher_staff_parking_transaction($id_device)
    {
        $this->db->order_by('created_at', 'desc');
        return $this->db->get_where('teacher_staff_parking_transactions', ['id_device' => $id_device])->result_array();
    }
}
