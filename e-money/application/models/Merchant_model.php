<?php
class Merchant_model extends CI_Model
{
    public function get_merchants()
    {
        $this->db->where(['merchants.deleted' => 0]);
        return $this->db->get('merchants')->result_array();
    }

    public function get_merchant($field, $data)
    {
        return $this->db->get_where('merchants', [$field => $data])->result_array();
    }

    public function insert(...$data)
    {
        $this->db->insert('merchants', $data[0]);
    }

    public function update($id_merchant, ...$data)
    {
        $this->db->update('merchants', $data[0], ['id_merchant' => $id_merchant]);
    }

    public function delete($id_merchant)
    {
        // $this->db->where('id_merchant', $id_merchant);
        // $this->db->delete('merchants');
        $this->db->update('merchants', ['deleted' => 1], ['id_merchant' => $id_merchant]);

    }
}
