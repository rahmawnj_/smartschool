<?php
class Product_model extends CI_Model
{
    public function get_products()
    {
        $this->db->select('products.*, merchants.nama as merchant_nama');
        $this->db->join('merchants', 'merchants.id_merchant = products.id_merchant');
        $this->db->join('categories', 'categories.id_category = products.id_category');
        $this->db->where(['products.deleted' => 0]);
        return $this->db->get('products')->result_array();
    }

    public function get_product($fields, $data)
    {
        $this->db->select('products.*, merchants.nama as merchant_nama, categories.*');
        $this->db->join('merchants', 'merchants.id_merchant = products.id_merchant');
        $this->db->join('categories', 'categories.id_category = products.id_category');
        $this->db->where(['products.deleted' => 0]);
        return $this->db->get_where('products', [$fields => $data])->result_array();
    }

    public function get_product_merchant($id_merchant, $fields, $data)
    {
        $this->db->select('products.*, merchants.nama as merchant_nama, categories.*');
        $this->db->join('merchants', 'merchants.id_merchant = products.id_merchant');
        $this->db->join('categories', 'categories.id_category = products.id_category');
        return $this->db->get_where('products', ['products.id_merchant' => $id_merchant ,$fields => $data])->result_array();
    }


    public function insert(...$data)
    {
        $this->db->insert('products', $data[0]);
    }

    public function update($id_product, ...$data)
    {
        $this->db->update('products', $data[0], ['id_product' => $id_product]);
    }

    public function delete($id_product)
    {
        // $this->db->where('id_product', $id_product);
        // $this->db->delete('products');
        $this->db->update('products', ['deleted' => 1], ['id_product' => $id_product]);

    }

    public function search_product($filter)
    {
        // $this->db->where('nama', $filter);
        // $this->db->or_where('nis', $filter);
        return $this->db->get('products')->result_array();

    }
}
