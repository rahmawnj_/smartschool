<?php

// menghubungkan ke database
$servername = "localhost";
$username = "smag5642";
$password = "iW5T4G98V7Av38";
$dbname = "smag5642_smart_school";

$conn = mysqli_connect($servername, $username, $password, $dbname);

// melakukan query ke tabel settings
$sql = "SELECT * FROM settings";
$result = mysqli_query($conn, $sql);

if ($result->num_rows > 0) {
    // mengambil data dari setiap baris hasil query
    while($row = $result->fetch_assoc()) {
        // menambahkan data ke array
        $settings[] = $row;
    }
}




// menutup koneksi ke database
mysqli_close($conn);

?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?= $settings[2]['value'] ?></title>
	<link rel="icon" type="image/png" href="<?= 'assets/img/uploads/' . $settings[3]['value'] ?>">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
	<style>
		.full-background {
			background-image: url('https://th.bing.com/th/id/OIP.oo8b8EY8YZoB4ZUv6o6ESwHaE8?pid=ImgDet&rs=1');
			background-size: cover;
			background-position: center;
			min-height: 100vh; /* tambahkan min-height */
			display: flex;
			flex-direction: column;
			justify-content: center;
			align-items: center;
		}
      

	</style>
</head>
<body>

	<div class="full-background">
		<div class="container">
		<div class="row">
      <div class="col-md-12">
      
        <h1 class="text-center mb-3 text-white"><?= $settings[2]['value'] ?></h1>
        <p class="text-center mb-5 text-white"><?= $settings[5]['value'] ?></p>
      </div>
    </div>
			<div class="row">
				<div class="col-md-4">
					<div class="card">
						<img src="E-MONEY.png" class="card-img-top" alt="Card Image 1">
						<div class="card-body">
							<h5 class="card-title text-center">E-Money</h5>
							<a href="https://e-money.smartschoolintegrated.com/" class="btn btn-primary">Go To e-money</a>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="card">
						<img src="E-LIBRARY.png" alt="Card Image 2">
						<div class="card-body">
							<h5 class="card-title text-center">E-Library</h5>
							<a href="https://e-library.smartschoolintegrated.com/" class="btn btn-primary">Go To e-library</a>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="card">
						<img src="E-ABSENSI.png" class="card-img-top" alt="Card Image 3">
						<div class="card-body">
							<h5 class="card-title text-center">E-Attendance</h5>
							<a href="https://e-attendance.smartschoolintegrated.com" class="btn btn-primary">Go To e-attendance</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>

</body>
</html>
